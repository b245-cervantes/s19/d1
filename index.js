console.warn("Hello World!");

// What is conditional statements?
// conditional Statement allow us to control
//the flow of our program.
//It allows us to run statement/instruction if a condition
// is met or run another sparte instruction if not otherwise.

//[SECTION] if, else if and else statement

let numA = -1
/**
 * if statement
 *   - it will execute the statement/code blocks
 *     if a specified condition is met/true
 * 
 * 
 * */

if(numA < 0){
    console.log("Hello from numA a");
}

console.log(numA < 0);

/**
 *   Sysntax:
 * if(condition){
 * 
 * statement;
 * 
 * } 
 * 
 */
// the resuslt of the expression added in the if's condition must result to true,
// else the statement with the same condition.

//lets reassign the variable numA and run if statement with the same condition

numA = 1;

if(numA < 0) {
    console.log("Hello from reassigned value of numA");
}

console.log(numA <0);

//It will not run because the expression now  results to false.

let city = "New york";

if(city === "New york"){
    console.log("Welcome to New york");
}

//else if clause
/**
 *    -Execute a statement of previous conditions
 *      are false and if the specified condition is true
 *    - the 'else if' is optional can be added to capture
 *      addtional condition to change the flow of a program.
 *
 */

let numH = 1;

if(numH <0) {
    console.log("Hello from numH < 0");
}else if(numH > 0){
    console.log("hello form numH > 0");
}
//we were able to run the else if() statement
// after we evaluated that the if condition was failed
// false.

// if the if() condition was passed and run, we will no longer evaluate
// to else if() and end the process.



// else if depeneded with if, you cannot use else if clause alone.
// {
//     else if (numH>0){
//         console.log("Hello form numH > 0")
//     }
// }

if(numH !== 1){
    console.log("Hello form nnumH === 1!");
} else if (numH === 1){
    console.log("hello from numH > 0!");
} else if(numH > 0){
    console.log("Hello from the second else if clause!");
}

city = "Tokyo";

if(city === "New york"){
    console.log("Welcome to New york");
}
else if (city === "Tokyo"){
    console.log("Welcome to tokyo!")
}

// else statement

/**
 *  -Execute a statement if all other conditions are false/ not met.
 *  - else if statement is optional and can be added to capture any other result
 *    change the flow of program.
 * 
 */

numH = 2;

if(numH<0) {
    console.log("Hello from if statement")
}
//false
else if(numH>2){
    console.log("Hello from the first else if")
}
//false
else if(numH>3){
    console.log("hello form the second else if");
}
//Since all of the conditon above are false /
// not met else statement will run.
else {
    console.log("hello from the if statement");
}

// Since all of the proceeding if and else 
//condition are not met, the else statement was 
// executed oinstead.
  //Else statement is also dependent with if statement, it 
  //cannot go alone.

//   {
//     else {
//         console.log("hello from the else inside the code block!");
//     }
//   }

     //if, else if and else statement with functions
     /**
      * most of the time we would like to use
      * if, else if and if statement with function to
      * control flow of our application
      */

     let message;

     function determineTyphoonIntesity(windSpeed){
        if(windSpeed<0){
            return "Invalid argument!";
        }
        else if(windSpeed <=30){
            return "Not a typhoon yet!";
        }
        else if(windSpeed <=60){
            return "Tropical depression detected!";
        } 
        else if(windSpeed <=88){
            return "Tropical storm Detected";
        }
        else if(windSpeed <=117){
            return "Severe Tropical storm Detected";
        }
        else if(windSpeed >= 118) {
            return "Typhoon Detected!";
        } else {
            return "invalid";
        }

    }

     let typeOfTyphoon = determineTyphoonIntesity(118);
     console.log(typeOfTyphoon);

      if(typeOfTyphoon === "Typhoon Detected!"){
        console.warn(typeOfTyphoon);
      }

    


//[Section] Truthy or falsy
	// Javascript a "truthy" is a value that is considered true when encountered in Boolean context
	//Values are considered true unless defined otherwise.

	// falsy values/ exceptions for truthy
	/*
		1. false
		2.0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN - not a number
	*/

    if(true){
        console.log("true");
    }
    
    if(1){
        console.log("true");
    }
    
    if(null){
        console.log("Hello from null inside the if condition.")
    }
    else{
    
        console.log("hello from the else of the null condition")
    

    }
//[SECTION] Condition operator/ ternary operator
     /**
      *  The conditional Operator
      *    1. condition
      *     2. Expression to execute if the condition
      *      is true or truthy
      *    3. expression if the condition is falsy
      *     
      *     Syntax:
      *    (expression) ? iftrue : ifFalse;
      */


     let ternaryResult = (1>18)? true : false;
     console.log(ternaryResult);


     //[SECTION] Switch Statement


     /** 
      * The switch statement evaluates an expression
      * and matches the expression' value to a case class
      *  */

     //tolowerCase() method will convert all the letters into small letters. 
     let day = prompt("What day of week today? ").lowe;


     switch(day){
        case 'monday':
            console.log("The color of the day is red!");
              //it means the code will stop here
           break;
        case 'tuesday': 
            console.log("The color of the day is orange!");
            break;
        case 'wednesday': 
            console.log("The color of the day is yellow!");
            break;
        case 'thursday': 
            console.log("The color of the day is green!");
            break;
        case 'friday': 
            console.log("The color of the day is blue!");
            break;
        case 'saturday': 
            console.log("The color of the day is inigo!");
            break;
        case 'sunday': 
            console.log("The color of the day is violet!");
            break;

        default: console.log("Please input valid day!");
            break;

     }

     //[SECTION] try-catch-finally
            //try-catch statement for error handling
            //there are instances when the application
            //return an error/warning taht is not
            //necessarily an error in the context of our
            // code.
            //these errors are result of an attempt of the programming language
            // to help developers in creating effecient code.

            function showIntensity(windSpeed){
                try {
                    alertat(determineTyphoonIntesity(windSpeed));
                    alert("Intensity updates will show alert.")
                }
                catch(error){
                    console.log(error.message);

                }
                finally{

                    alert("Intensity updates will show new alert from finally");
                }
            }

            showIntensity(119);